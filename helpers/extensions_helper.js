/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

'use strict';

String.prototype.replaceAll = function(find, replace) {
  if( find.constructor !== Array )
    find = [find];
  
  var replaceString = this;

  for (var i = 0; i < find.length; i++)
    replaceString = replaceString.replace(find[i], replace);
  
  return replaceString;
}