/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

'use strict';

import {
  Dimensions
} from 'react-native';

export default class Screen {

	static width() {
		return Dimensions.get('window').width;
	}

	static height() {
		return Dimensions.get('window').height;
	}

  static factor(width) {
    return Screen.width() / width;
  }

  static center() {
    const dimensions = Dimensions.get('window');
    return { x:dimensions.width / 2, y:dimensions.height / 2 };
  }

}