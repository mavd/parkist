/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

'use strict';

// extensions
require('./extensions_helper.js');

// translations
const I18n = require('../assets/i18n.js');

export default class Data {
  static print(key, data = 'not_set') {
    if( data === 'not_set' ) {
      data = key;
      key = null;
    }

    //try {
    //  console.log(key + ': ' + JSON.stringify(data));
    //} catch(e) {
      if(key)
        console.log(key + ': ');
      console.log(data);
      console.log('------')
    //}
  }

  static get(data, key, defaultValue = null) {
    if( typeof data === 'undefined' || data === null )
      return defaultValue;

    if( typeof key === 'undefined' || key === null )
      return dataultValue;

    if( key.constructor && key.constructor === Array ) {
      if( key.length === 0 )
        return data;

      data = Data.get(data, key.shift(), defaultValue);
      return Data.get(data, key, defaultValue);
    }

  	return ( typeof data[key] !== 'undefined' && data[key] !== null ) ? data[key] : defaultValue;
  }

  static copy(data) {
  	return Object.assign({}, data);
  }

  static geocodeResultsPrepare(geocodeResults) {
    let results = [];
    let usedResults = [];

    for( let i = 0; i < geocodeResults.length; i++ ) {
      const geocodeResult = geocodeResults[i];
      let result = {};

      // first address, readable format 
      const formattedAddress = Data.get(geocodeResult, 'formattedAddress', '').replaceAll(I18n.t('address_replace_key'), '').trim();
      const country = Data.get(geocodeResult, 'country', '');
      const adminArea = Data.get(geocodeResult, 'adminArea', '').replaceAll(I18n.t('address_replace_key'), '').trim();
      const locality = Data.get(geocodeResult, 'locality', '');
      const subAdminArea = Data.get(geocodeResult, 'subAdminArea', '').replaceAll(I18n.t('address_replace_key'), '').trim();
      const subLocality = Data.get(geocodeResult, 'subLocality', '');
      const latitude = Data.get(geocodeResult, ['position', 'lat']);
      const longitude = Data.get(geocodeResult, ['position', 'lng']);

      if( latitude && longitude ) {
        result.latitude = latitude;
        result.longitude = longitude;
      }

      let area = [];
      if( address !== country && country.length )
        area.push(country);
      if( address !== adminArea && area.indexOf(adminArea) === -1 && adminArea.length )
        area.push(adminArea);
      if( address !== subAdminArea && area.indexOf(subAdminArea) === -1 && subAdminArea.length )
        area.push(subAdminArea);
      if( address !== locality && area.indexOf(locality) === -1 && locality.length )
        area.push(locality);
      if( address !== subLocality && area.indexOf(subLocality) === -1 && subLocality.length )
        area.push(subLocality);

      result.area = area.join(', ');

      // first address segment (street)
      let address = [];
      let addressComponents = formattedAddress.split(',').map(function (str) { return str.trim() });
      address.push(addressComponents[0]);
      if( addressComponents.length > 1 && area.indexOf(addressComponents[1]) === -1 )
        address.push(addressComponents[1]);
      result.address = address.join(' ');

      area.push(address);
      area = area.join(',');

      if( usedResults.indexOf(area) !== -1 )
        continue;

      usedResults.push(area);
      results.push(result);

      // we don't need more then 5 results
      if( results.length == 5 )
        break;
    }

    //Data.print('DataHelper.geocodeResultsPrepare', results);

    return results;
  }

}