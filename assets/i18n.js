/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import I18n from 'react-native-i18n'

I18n.fallbacks = true

I18n.translations = {
  en: {
  	// scene titles
  	parkist_scene_title: '',
  	request_scene_title: 'Summon parkist',
  	refine_scene_title: 'Enter precise address',

  	// map scene
    geolocation_not_permitted: 'Geolocation disabled. You can permit geolocation in settings',
    refine_address: 'Change address',
  	summon_parkist: 'Summon parkist',
  	contact_phone: 'Make a call',
  	contact_email: 'Write email',
  	contact_cancel: 'Cancel',

  	contact_phone_not_set: 'Contact phone is not set',
  	contact_email_not_set: 'Contact email is not set',
  	contact_email_subject: 'Parkist app feedback',
  	
  	// request scene
  	label_address: 'Address',
  	label_coords: '',
    label_link: '',
  	label_type: 'Vehicle type',
  	label_name: 'Name',
  	label_phone: 'Phone',
  	label_time: 'Due time',
  	label_comment: 'Comment',
  	due_time_now: 'Now',
  	form_submit: 'Submit',
  	parkist_request_created: 'Request created successfully, wait for parkist please',
  	parkist_request_error: 'Request was not created, try later please',
    
    // address refine scene
    address_default_prefix: 'Russia Moscow ',
    address_not_found: 'Address not found',

    // address key to erase
    address_replace_key: 'street ',
  },

  ru: {
  	// заголовки экранов
  	parkist_scene_title: '',
  	request_scene_title: 'Вызвать паркиста',
  	refine_scene_title: 'Введите точный адрес',

  	// экран карты
    geolocation_not_permitted: 'Геолокация отключена. Вы можете разрешить геолокацию в настройках',
  	refine_address: 'Изменить адрес',
  	summon_parkist: 'Вызвать паркиста',
  	contact_phone: 'Позвонить',
  	contact_email: 'Написать на почту',
  	contact_cancel: 'Отмена',

  	contact_phone_not_set: 'Телефон администрации не установлен',
  	contact_email_not_set: 'Эл. почта администрации не установлена',
  	contact_email_subject: 'Обратная связь из приложения Паркист',
  	
  	// экран заявки
  	label_address: 'Адрес',
  	label_coords: '',
    label_link: '',
  	label_type: 'Марка и номер автомобиля',
  	label_name: 'Имя',
  	label_phone: 'Телефон',
  	label_time: 'Время встречи',
  	label_comment: 'Комментарий, пожелания',
  	due_time_now: 'Ближайшее',
  	form_submit: 'Отправить',
  	parkist_request_created: 'Заявка создана, пожалуйста ожидайте прибытия паркиста',
  	parkist_request_error: 'Заявку не получилось создать, пожалуйста попробуйте позже',

    // экран уточнения адреса
    address_default_prefix: 'Россия Москва ',
    address_not_found: 'Адрес не найден',
    
    // убираем из строки адреса
    address_replace_key: ['улица ', 'ул. ', 'город '],
  }
}

module.exports = I18n;