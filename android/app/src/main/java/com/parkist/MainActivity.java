package com.parkist;

import android.os.Bundle;

import com.facebook.react.ReactActivity;

public class MainActivity extends ReactActivity {

  /**
   * Returns the name of the main component registered from JavaScript.
   * This is used to schedule rendering of the component.
   */
  @Override
  protected String getMainComponentName() {
      return "Parkist";
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    _instance = this;
  }

  /**
   * Bullshit to enable react-native-actionsheet-native
   */
  static private MainActivity _instance;

  public static MainActivity getInstance() {
    return _instance;
  }

}
