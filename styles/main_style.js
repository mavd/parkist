/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import {
  StyleSheet,
} from 'react-native';

require('../config/constants_config.js')

module.exports = StyleSheet.create({

  defaultContainer: {
    flex: 1,
    backgroundColor: BACKGROUND_COLOR, // map background color
  },

  messageContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    minHeight: VERTICAL_SPACING * 6,
  },

  message: {
    opacity: OPACITY * .8,
    fontSize: REGULAR_FONT_SIZE,
  },

  header: {
    height: HEADER_HEIGHT,
    left: 0,
    right: 0,
    position: 'absolute',
    zIndex: 1,
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'space-between',
    paddingLeft: 15,
    paddingRight: 15,
    paddingBottom: VERTICAL_SPACING,
    borderColor: BORDER_COLOR,
    borderBottomWidth: 1,
  },

  mapHeader: {
    borderBottomWidth: 0,
  },

  headerButton: {
    fontSize: 25,
    backgroundColor: 'transparent',
    color: BUTTON_COLOR,
    width: HEADER_BUTTON_SIZE,
    height: HEADER_BUTTON_SIZE,
    textAlign: 'center',
    lineHeight: HEADER_BUTTON_SIZE,
  },

  title: {
    fontSize: BUTTON_FONT_SIZE,
    backgroundColor: 'transparent',
    height: 32,
    textAlign: 'center',
    textShadowColor: SHADOW_LIGHT_COLOR,
    textShadowOffset: {width: 1, height: 1},
    textShadowRadius: 1,
    opacity: OPACITY,
  },

  addressWrapper: {
    backgroundColor: 'transparent',
    top: CONTAINER_VERTICAL_PADDING,
    left: 0,
    right: 0,
    position: 'absolute',
    zIndex: 1,
  },

  address: {
    paddingLeft: 25,
    paddingRight: 25,
    fontSize: ADDRESS_FONT_SIZE,
    textAlign: 'center',
    textShadowColor: SHADOW_LIGHT_COLOR,
    textShadowOffset: {width: 1, height: 1},
    textShadowRadius: 1,
    opacity: OPACITY,
  },

  buttonAddress: {
    paddingTop: VERTICAL_SPACING,
    fontSize: ADDRESS_FONT_SIZE * .85,
    opacity: OPACITY * 1.1,
    color: BUTTON_COLOR,
  },

  map: {
    flex: 1,
    alignSelf: 'stretch',
    backgroundColor: 'transparent',
    borderWidth: 0,
    opacity: OPACITY * .8,
    alignItems: 'center',
    justifyContent: 'center',
  },

  marker: {
    fontSize: MARKER_SIZE,
    opacity: OPACITY * 1.1,
    //color: buttonColor,
    //top: -markerSize,
    width: MARKER_SIZE,
    textAlign: 'center',
    position: 'absolute',
    zIndex: 1,
    left: SCREEN_WIDTH / 2 - MARKER_SIZE / 2,
    top: SCREEN_HEIGHT / 2 - MARKER_SIZE / 2,
    backgroundColor: 'transparent',
  },

  submitButtonWrapper: {
    flex: 1,
    alignItems: 'center',
  },

  parkistButtonWrapper: {
    position: 'absolute',
    bottom: CONTAINER_VERTICAL_PADDING,
    left: 0,
    right: 0,
    zIndex: 1,
  },

  submitButton: {
    borderRadius: 5,
    backgroundColor: BUTTON_COLOR,
    paddingTop: VERTICAL_SPACING,
    paddingBottom: VERTICAL_SPACING,
    paddingLeft: HORIZONTAL_SPACING,
    paddingRight: HORIZONTAL_SPACING,

    flexDirection: 'row',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',

    shadowColor: SHADOW_COLOR,
    shadowOffset: {width: 2, height: 2},
    shadowRadius: 4,
    shadowOpacity: 1,
  },

  requestContainer: {
    flex: 1,
    backgroundColor: BACKGROUND_COLOR,
    borderLeftWidth: 1,
    borderColor: BORDER_COLOR,
    
    paddingTop: HEADER_HEIGHT,
  },

  requestButton: {
    marginTop: VERTICAL_SPACING * 2,
    marginBottom: VERTICAL_SPACING * 4,
  },

  buttonIcon: {
    backgroundColor: 'transparent',
    marginRight: 5,
    opacity: .8,
  },

  buttonText: {
    color: BUTTON_TEXT_COLOR,
    fontSize: BUTTON_FONT_SIZE,
    backgroundColor: 'transparent',
    opacity: .9,
  },

  scroll: {
    padding: HORIZONTAL_SPACING,
  },

  requestIndicator: {
    marginTop: VERTICAL_SPACING * 2,
  },

  addressContainer: {
    flex: 1,
    backgroundColor: BACKGROUND_COLOR,
    borderTopWidth: 1,
    borderColor: BORDER_COLOR,
    
    paddingTop: VERTICAL_SPACING * 3,
  },

  addressFormWrapper: {
    paddingLeft: HORIZONTAL_SPACING,
    paddingRight: HORIZONTAL_SPACING,
    paddingTop: VERTICAL_SPACING,
    borderBottomWidth: 1,
    borderColor: BORDER_COLOR,
  },

  addressList: {
    flex: 1,
    paddingLeft: HORIZONTAL_SPACING,
    paddingRight: HORIZONTAL_SPACING,
    overflow: 'visible',
  },

  addressListItem: {
    paddingTop: VERTICAL_SPACING,
    paddingBottom: VERTICAL_SPACING,
  },

  addressListItemAddress: {
    fontSize: REGULAR_FONT_SIZE,
  },

  addressListItemArea: {
    fontSize: REGULAR_FONT_SIZE * .85,
    opacity: OPACITY,
  },
  
  // Utility

  invisible: {
    opacity: 0,
  },

  hidden: {
    width: 0,
    height: 0,
  },

  defaultShadow: {
    textShadowColor: SHADOW_COLOR,
    textShadowOffset: {width: 2, height: 2},
    textShadowRadius: 4,
  },

});