/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

'use strict';

// data helper
import Data from '../helpers/data_helper';
import RemoteModel from './remote_model';

const type = 'tasks';

export default class TaskModel extends RemoteModel {

  getType() {
    return type;
  }

}