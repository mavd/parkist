/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

'use strict';

// data helper
import Data from '../helpers/data_helper';
import { AsyncStorage } from 'react-native';
import RemoteModel from './remote_model';

const type = 'contacts';

export default class ClientModel extends RemoteModel {

  check(phone) {
    return new Promise(function(resolve) {
      AsyncStorage
        .getItem('client_' + phone, (error, clientId) => {
          //Data.print('ClientModel.check.AsyncStorage.getItem Error', error);
          //Data.print('ClientModel.check.AsyncStorage.getItem', clientId);

          return clientId ? resolve({ 'id': clientId }) : resolve(null);
        })
    })
  }

  get(phone) {
    //Data.print('ClientModel.get Phone', phone);

    return this
      .check(phone)
      .then((client) => {
        return client ? client : super.get(phone);
      }) 
  }

  getType() {
    return type;
  }

  getOrCreate(data) {
    const phone = Data.get(data, ['custom_fields', 0, 'values', 0, 'value']);

    //Data.print('ClientModel.getOrCreate', this.get(phone));
    //Data.print('ClientModel.getOrCreate.then', this.get(phone).then((client) => { return client ? client : super.create(data) }));

    return this
      .get(phone)
      .then((client) => {
        //Data.print('ClientModel.getOrCreate Client', client);

        return client ? client : super.create(data)
      })
      .then((client) => {
        const clientId = Data.get(client, 'id');
        if( clientId )
          AsyncStorage
            .setItem('client_' + phone, String(clientId), function(error) {
              //Data.print('ClientModel.getOrCreate.AsyncStorage.setItem Error', error);
            });

        return client;
      })
      .catch((error) => {
        Data.print('Client model getOrCreate error', error);
        
        return null;
      })
  } 

}