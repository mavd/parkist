/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

'use strict';

// data helper
import Data from '../helpers/data_helper';

// cookies
//const CookieManager = require('react-native-cookies');

// auth stuff
const user_login = 'igamer@mail.ru';
const user_hash = '128507635fb676b3188a1df08fc9a1ac';
const api_url = 'https://new58a59f6e8a668.amocrm.ru/';

const endpoints = {
	login_endpoint: 'private/api/auth.php?type=json',
	get_endpoint: 'private/api/v2/json/{type}/list',
	add_endpoint: 'private/api/v2/json/{type}/set',
}

export default class RemoteModel {

	reauthentication = false

	login() {
		const self = this;

		// Try to pass hash and login always unstead of pre authorisation
		return new Promise(function(resolve) {
			return resolve(true);		
		});

		/*
		return new Promise(function(resolve) {
			// Check for cookies
			CookieManager.get(api_url, (err, res) => {
				if( Data.get(res, 'BITRIX_SM_LOGIN') )
			  	return resolve(true);

			  const options = {
				  method: 'POST',
				  headers: {
				    'Accept': 'application/json',
				    'Content-Type': 'application/json',
				  },
				  body: JSON.stringify({
				    USER_LOGIN: user_login,
				    USER_HASH: user_hash,
				  }),
				  timeout: 10,
				};

				var endpoint = endpoints['login_endpoint'];

				Data.print('Remote model login endpoint', endpoint);

		  	return fetch(api_url + endpoint, options)
		      .then((response) => response.json())
		      .then((responseJson) => {
		      	const isAuthenticated = Data.get(responseJson, ['response', 'auth'], null);

		      	self.reauthentication = false;
		        
		        return isAuthenticated;
		      })
		      .catch((error) => {
		        Data.print('Remote model login error', error);
		        
		        return null;
		      })
			})
    })*/
  }

  // GET ELEMENT
  get(query) {
  	const type = this.getType();
  	const options = {
		  method: 'GET',
		  headers: {
		    'Accept': 'application/json',
		    'Content-Type': 'application/json',
		  },
		  //timeout: 10,
		};

		return this
  		.login()
  		.then((isAuthenticated) => {
  			if( !isAuthenticated )
          throw 'Authentication failed';

        var endpoint = api_url + endpoints['get_endpoint'].replace('{type}', type) + '?limit_rows=1&query=' + query + '&USER_HASH=' + user_hash + '&USER_LOGIN=' + user_login;

        Data.print('Remote model get endpoint', endpoint);

  			return fetch(endpoint, options);
  		})
  		
  		.then((response) => {
  			//Data.print('Remote model get' + (this.reauthentication ? ' (reauthentication)' : ''), response);

  			return response.json();
  		})
      
      .then((responseJson) => {
      	//Data.print('Remote model get json' + (this.reauthentication ? ' (reauthentication)' : ''), responseJson);

      	/*
      	if( Data.get(responseJson, ['response', 'error_code']) == 110 && !Data.get(this, 'reauthentication') ) {
        	this.reauthentication = true;
        	CookieManager.clearAll((err, res) => {
					  Data.print('Cookies cleared!');
					});

        	return this.get(query);
        }*/

        return Data.get(responseJson, ['response', type, 0]);
      })
      .catch((error) => {
        Data.print('Remote model get error', error);
        
        return null;
      })
  }

  getCreateMethod() {
  	return 'add';
  }

  // CREATE ELEMENT
  create(data) {
  	const type = this.getType();
  	const method = this.getCreateMethod();
  	const options = {
		  method: 'POST',
		  headers: {
		    'Accept': 'application/json',
		    'Content-Type': 'application/json',
		  },
		  body: JSON.stringify({
		  	'request': {
		  		[type]: {
		  			[method]: [data]
		  		} 
		  	},

		  	USER_LOGIN: user_login,
				USER_HASH: user_hash,
		  }),
		  timeout: 10,
		};

  	return this
  		.login()
  		.then((isAuthenticated) => {
  			if( !isAuthenticated )
          throw 'Authentication failed';

        var endpoint = api_url + endpoints['add_endpoint'].replace('{type}', type) + '?USER_HASH=' + user_hash + '&USER_LOGIN=' + user_login;

				Data.print('Remote model create endpoint', endpoint);
				//Data.print('Remote model create body', options.body);

  			return fetch(endpoint, options);
  		})
      .then((response) => {
  			Data.print('Remote model create' + (this.reauthentication ? ' (reauthentication)' : ''), response);

  			return response.json();
  		})
      .then((responseJson) => {
      	Data.print('Remote model create json' + (this.reauthentication ? ' (reauthentication)' : ''), responseJson);

        /*
        if( Data.get(responseJson, ['response', 'error_code']) == 110 && !Data.get(this, 'reauthentication') ) {
        	this.reauthentication = true;
        	CookieManager.clearAll((err, res) => {
					  Data.print('Cookies cleared!');
					});

        	return this.create(data);
        }*/

        return Data.get(responseJson, ['response', type, 'add', 0]);
      })
      .catch((error) => {
      	Data.print('Remote model create error', error);
        
        return null;
      })
  }

}