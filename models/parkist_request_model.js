/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

'use strict';

// data helper
import Data from '../helpers/data_helper';

import RemoteModel from './remote_model';
import ClientModel from './client_model';
import ClientToParkistRequestModel from './client_to_parkist_request_model';
import TaskModel from './task_model';

// no ass time
const moment = require('moment');

const type = 'leads';

export default class ParkistRequestModel extends RemoteModel {

  getType() {
    return type;
  }

  create(data) {
    var currentClient = null;

    return this
      .login()
      .then((isAuthenticated) => {
        if( !isAuthenticated )
          throw 'Authentication failed';

        const client = {
          name: data.name,
          custom_fields: [
            
            { // Телефоны
              id: 124040,
              values: [
                {
                  value: data.phone,
                  enum: 'MOB'
                },
              ]
            }
          ]
        }

        return new ClientModel().getOrCreate(client);
      })
      .then((client) => {
        if( !client )
          throw 'Client get/create failed';

        currentClient = client

        //Data.print('Parkist request model client', client);

        const lead = {
          name: 'Вызов паркиста ' + moment().format('X'),
          status_id: '13898946',
          //linked_company_id: client.id,
          
          custom_fields: [
            
            { // Адрес
              id: 127242,
              values: [
                {
                  value: data.address,
                },
              ]
            },

            { // Координаты
              id: 127244,
              values: [
                {
                  value: data.coords,
                },
              ]
            },

            { // Ссылка
              id: 170454,
              values: [
                {
                  value: data.mapLink,
                },
              ]
            },

            { // Тип
              id: 127246,
              values: [
                {
                  value: data.type,
                },
              ]
            },

            { // Время
              id: 127252,
              values: [
                {
                  value: data.time,
                },
              ]
            },

            { // Комментарий
              id: 127254,
              values: [
                {
                  value: data.comment,
                },
              ]
            },

          ]
        }

        return super.create(lead);
      })
      .then((lead) => {
      	Data.print('ParkistRequestModel.create Link lead', lead);

        const link = {
          from: 'contacts',
          from_id: currentClient.id,
          to: 'leads',
          to_id: lead.id
        }

        return new ClientToParkistRequestModel()
          .create(link)
          .then(() => {
            return lead;
          })
      })
      .then((lead) => {
        const task = {
          element_id: lead.id, 
          element_type: 2, // lead
          task_type: 1, // call
          text: lead.name,
          complete_till: moment().add(10, 'minutes').format('X'),
        }

        return new TaskModel()
          .create(task)
          .then(() => {
            return lead;
          })
      })
      .catch((error) => {
        Data.print('Parkist request model create error', error);
        
        return null;
      })
  }

}