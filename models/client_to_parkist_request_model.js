/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

'use strict';

// data helper
import Data from '../helpers/data_helper';

import RemoteModel from './remote_model';

const type = 'links';

export default class ClientToParkistRequestModel extends RemoteModel {

  getType() {
    return type;
  }

  getCreateMethod() {
    return 'link';
  }

}