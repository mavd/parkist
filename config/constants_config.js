/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

'use_strict';

// data helper
import Screen from '../helpers/screen_helper';
import Platform from 'react-native';

// factor for rescale relative to base size for screen width 375
const factor = Screen.factor(375);

global.BACKGROUND_COLOR = 'white';//'rgb(249, 245, 237)' // merino
global.BUTTON_COLOR = 'rgb(19, 81, 159)'; // toryBlue
global.BORDER_COLOR = 'rgba(19, 81, 159, .25)'; // toryBlue
global.BUTTON_TEXT_COLOR = 'rgb(255, 255, 255)'; // white
global.SHADOW_COLOR = 'rgba(22, 27, 34, .5)'; // bunker
global.SHADOW_LIGHT_COLOR = 'rgb(150, 150, 150)'; // heather

global.SCREEN_WIDTH = Screen.width();
global.SCREEN_HEIGHT = Screen.height();
global.OPACITY = .7;
global.VERTICAL_SPACING = 10;
global.HORIZONTAL_SPACING = 20;
global.HEADER_HEIGHT = 75;
global.MARKER_SIZE = 75 * factor;
global.ADDRESS_FONT_SIZE = 25 * factor;
global.BUTTON_FONT_SIZE = 20 * factor;
global.REGULAR_FONT_SIZE = 16 * factor;
global.HEADER_BUTTON_SIZE = 40;
global.CONTAINER_VERTICAL_PADDING = 100 * factor;

global.CONTACT_PHONE = '+7(495)506-76-18';
global.CONTACT_EMAIL = 'info@parkist.ru';

global.GOOGLE_API_KEY = Platform.OS === 'ios' ? 'AIzaSyAG4xk8QOGdjR0tIdJaDvZ2_dQRkTRWC5Q' : 'AIzaSyD3cVDjhfG1_4OcqhbrWS5Im31f0NGimt8';