/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
'use strict';

import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ListView,
  ActivityIndicator,
  Alert
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Geocoder from 'react-native-geocoder';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

// form import
import {
  form,
  String as fString,

  maybe,
  struct,
} from 'tcomb-form-native';

// data helper
import Data from '../helpers/data_helper';

// translations
const I18n = require('../assets/i18n.js');

// main style
const style = require('../styles/main_style.js');

// form style
const formStyle = require('../styles/form_style.js');

// form component
const Form = form.Form;

// address form model
const Address = struct({
  address: maybe(fString),
});

// address list data source
const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1.address !== r2.address && r1.area !== r2.area });

export default class ParkistAddressRefine extends Component {
   // form options
  options = {
    fields: {
      address: {
        label: ' ',
        
        onSubmitEditing: (event) => this.onSubmit() 
      },
    },

    stylesheet: formStyle,
  }

  constructor(props) {
    super(props);
    
    this.state = {
      container: {},
      addressList: ds.cloneWithRows([]),
      isGeocodingRunning: true,
    }

    Geocoder.fallbackToGoogle(GOOGLE_API_KEY);
    Geocoder.alwaysUseGoogle();
  }

  componentDidMount() {
    this.setState({ value: this.props.data });
    this._form.getComponent('address').refs.input.focus();

    this.geocodeForward(this.props.data.address);
  }

  // get address list by address
  geocodeForward(address) {
    address = address.trim();

    Data.print('ParkistAddressRefine.geocodeForward', address);

    if( !address || !address.length ) {
      this.setState({ addressList: ds.cloneWithRows([]) });
      return;
    }

    // prevent multiple requests
    if( Data.get(this, 'lastAddressRequest') ) {
      this.lastAddressRequest = address;  
      return;
    }

    this.lastAddressRequest = address;
    this.setState({ isGeocodingRunning: true });
    
    // run request
    Geocoder.geocodeAddress(I18n.t('address_default_prefix') + address, I18n.locale).then(res => {
        // prevent multiple requests
        if(this.lastAddressRequest !== address) {
          address = this.lastAddressRequest;
          this.lastAddressRequest = null;
          return this.geocodeForward(address);
        }

        this.lastAddressRequest = null;

        Data.print('ParkistAddressRefine.geocodeForward.geocodeAddress', res);
        res = Data.geocodeResultsPrepare(res);
        
        // update state
        let state = {};
        state.isGeocodingRunning = false;
        // if( res.length > 0 ) // update only when there are results
          state.addressList = ds.cloneWithRows(res);
        
        //Data.print('ParkistAddressRefine.geocodeForward.geocodeAddress', state.addressList);

        this.setState(state);
    })
    .catch(err => {
      Data.print('ParkistAddressRefine.geocodeForward Error', err);

      this.lastAddressRequest = null;
      this.setState({ isGeocodingRunning: false, addressList:ds.cloneWithRows([]) });
    })
  }

  // form change
  onChange(value) {
    //Data.print('ParkistAddressRefine.onChange', value);

    let address = Data.get(value, 'address');
    if( address !== this.state.address )
      this.geocodeForward(address);

    this.setState({value});
  }

  // form submit
  onSubmit() {
    const data = this.state.addressList.getRowData(0, 0);
    
    if ( typeof data === 'undefined' || !data)
      return Alert.alert('', I18n.t('address_not_found'));;

    this.props.onReturn(data);
  }

  // form close
  onClose() {
    const data = this._form.getValue();
    
    this.props.onReturn(data);
  }

  // user selected address list item
  addressListItemPressed(data) {
    //Data.print('ParkistAddressRefine.addressListItemPressed', data);
    
    this.props.onReturn(data); 
  }

  // show form
  render() {
    
    return (

      <View style={[style.defaultContainer, style.addressContainer, this.state.container]}>
        
        {/* HEADER */}
        <View style={[style.header, style.mapHeader]}>
          <Icon name="question" style={[style.headerButton, style.invisible]} />

          <Text style={style.title}>{this.props.title}</Text>

          <TouchableOpacity onPress={ () => this.onClose() }>
            <Icon name="times" style={[style.headerButton, style.defaultShadow]} />
          </TouchableOpacity>
        </View>

        {/* FORM */}
        <View style={style.addressFormWrapper}>
          <Form
            type={Address}
            options={this.options}
            value={this.state.value}
            onChange={(value) => this.onChange(value)}
            ref={(form) => { this._form = form}} />
        </View>

        {/* GEOCODE RESULTS */}
        <KeyboardAwareScrollView 
          keyboardShouldPersistTaps='handled'
          enableAutoAutomaticScroll={false}
          showsVerticalScrollIndicator={false} >
        
          { this.state.isGeocodingRunning &&

          <ActivityIndicator
            style={style.requestIndicator}
            color={'#13519f'}
            animating={true}
            size="large" />

          }

          { !this.state.isGeocodingRunning && this.state.addressList.getRowCount(0) > 0 &&
          
          <ListView
            style={style.addressList}
            dataSource={this.state.addressList}
            enableEmptySections={true}
            keyboardShouldPersistTaps='handled'
            renderRow={(data) => 
              <TouchableOpacity style={style.addressListItem} onPress={() => this.addressListItemPressed(data)}>
                <Text style={style.addressListItemAddress}>{data.address}</Text>
                <Text style={style.addressListItemArea}>{data.area}</Text>
              </TouchableOpacity>
            } />

          }

          { !this.state.isGeocodingRunning && this.state.addressList.getRowCount(0) === 0 &&
          
          <View style={style.messageContainer}>
            <Text style={style.message}>{I18n.t('address_not_found')}</Text>
          </View>

          }

        </KeyboardAwareScrollView>

      </View>

    )
  }
}