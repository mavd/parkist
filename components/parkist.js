/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
'use strict';

import React, { Component } from 'react';
import {
  AppRegistry,
  Navigator,
  View,
  Text,
} from 'react-native';

// map component
import ParkistMap from './parkist_map';

// map refine component
import ParkistAddressRefine from './parkist_address_refine';

// request component
import ParkistRequest from './parkist_request';

// data helper
import Data from '../helpers/data_helper';

// main style
const style = require('../styles/main_style.js');

// translations
const I18n = require('../assets/i18n.js');

// scene index
const MAP_SCENE = 0;
const REQUEST_SCENE = 1;
const ADDRESS_REFINE_SCENE = 2;

// scenes for routing
let routes = [
  {
    title: I18n.t('parkist_scene_title'),
    index: MAP_SCENE,
  },

  {
    title: I18n.t('request_scene_title'), 
    index: REQUEST_SCENE,
  },

  {
    title: I18n.t('refine_scene_title'), 
    index: ADDRESS_REFINE_SCENE,
    animation: Navigator.SceneConfigs.FloatFromBottom
  }
]

export default class Parkist extends Component {
  onReturn(navigator, data) {
    this._parkistMap.addressRefined(data);

    navigator.pop();
  }

  onParkistRequest(navigator, data) {
    let route = routes[REQUEST_SCENE];
    route.data = data;
    navigator.push(route);
  }

  onAddressRefine(navigator, data) {
    let route = routes[ADDRESS_REFINE_SCENE];
    route.data = data;
    navigator.push(route);
  }

  renderScene(route, navigator) {
    //Data.print('Parkist.renderScene', route);

    switch( route.index ) {
      case MAP_SCENE:
        return <ParkistMap 
          title={route.title}
          navigator={navigator}
          ref={(parkistMap) => {this._parkistMap = parkistMap}}
          onRequest={(data) => this.onParkistRequest(navigator, data)}
          onAddressRefine={(data) => this.onAddressRefine(navigator, data)} />

      case REQUEST_SCENE:
        return <ParkistRequest
          title={route.title}
          data={route.data}
          navigator={navigator}
          onReturn={(data) => this.onReturn(navigator, data)} />

      case ADDRESS_REFINE_SCENE:
        return <ParkistAddressRefine
          title={route.title}
          data={route.data}
          navigator={navigator}
          onReturn={(data) => this.onReturn(navigator, data)} />
    }
  }

  render() {
    return (
      <Navigator
        initialRoute={routes[0]}
        renderScene={(route, navigator) => this.renderScene(route, navigator)}
        configureScene={(route, navigator) => Data.get(route, 'animation', Navigator.SceneConfigs.PushFromRight)} />
    )
  }
}

AppRegistry.registerComponent('Parkist', () => Parkist);