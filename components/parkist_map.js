/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
'use strict';

import React, { Component } from 'react';
import {
  Platform,
  View,
  TouchableOpacity,
  Text,
  Image,
  Alert,
} from 'react-native';
import MapView from 'react-native-maps';
import Icon from 'react-native-vector-icons/FontAwesome';
import Geocoder from 'react-native-geocoder';
import Communications from 'react-native-communications';
import ActionSheet from 'react-native-action-sheet';

// constants
require('../config/constants_config.js');

// data helper
import Data from '../helpers/data_helper';
import Screen from '../helpers/screen_helper';

// main style
const style = require('../styles/main_style.js');

// translations
const I18n = require('../assets/i18n.js');

// startin location
const oStartLocation = { // Moscow
  latitude: 55.751244,
  longitude: 37.618423,
  latitudeDelta: 0.0922,
  longitudeDelta: 0.0421
};

export default class ParkistMap extends Component {
  constructor(props) {
    //Data.print('ParkistMap.constructor');

    super(props);
    
    this.state = {
      address: '',
      isMapDraggedManually: false,
    };

    this.mapPosition = oStartLocation;

    Geocoder.fallbackToGoogle(GOOGLE_API_KEY);
    Geocoder.alwaysUseGoogle();
  }

  // user position
  initialUserPosition: null;
  lastUserPosition: null;

  // geolocation updates notifier
  watchID: ?number = null;

  // try to start watching user location
  componentDidMount() {
    //Data.print('ParkistMap.componentDidMount');

    navigator.geolocation.getCurrentPosition(
      (initialUserPosition) => {
        this.initialUserPosition = Data.get(initialUserPosition, 'coords');
      },

      (error) => {
        if( error.PERMISSION_DENIED === 1 )
          Alert.alert('', I18n.t('geolocation_not_permitted'));

        console.log(error);
      },
      
      {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000}
    );

    this.watchID = navigator.geolocation.watchPosition((lastUserPosition) => {
      this.lastUserPosition = Data.get(lastUserPosition, 'coords');
    });
  }

  // stop geolocation updates
  componentWillUnmount() {
    navigator.geolocation.clearWatch(this.watchID);
  }

  // move map programmatically
  moveMapTo(mapDestination, noAddressUpdate = false) {
    this.noAddressUpdate = noAddressUpdate;

    //Data.print('Перемещаем карту на', mapDestination);

    // center map on marker pointy bit unstead of actual map center
    mapDestination.latitude += this.mapPositionCenter.latitude - this.mapPosition.latitude;

    this._map._component.animateToCoordinate(mapDestination, 1000);
    if( !noAddressUpdate ) this.setState({
      address: '',
    })
  } 

  // final location changes
  onRegionChange(mapPosition) {
    var mapPositionNew = {};
    mapPositionNew.latitude = mapPosition.latitude;
    mapPositionNew.longitude = mapPosition.longitude;
    mapPositionNew.latitudeDelta = mapPosition.latitudeDelta;
    mapPositionNew.longitudeDelta = mapPosition.longitudeDelta;

    //Data.print('Карта двигается на', mapPositionNew);

    this.mapPosition = mapPositionNew;
    this.mapPositionCenter = mapPositionNew;
  }

  // final location changes
  onRegionChangeComplete(mapPosition) {
    Data.print('ParkistMap.js.onRegionChangeComplete');

    var mapPositionNew = {};
    mapPositionNew.latitude = mapPosition.latitude;
    mapPositionNew.longitude = mapPosition.longitude;
    mapPositionNew.latitudeDelta = mapPosition.latitudeDelta;
    mapPositionNew.longitudeDelta = mapPosition.longitudeDelta;

    //Data.print('Карта остановилась на', mapPositionNew);

    this.mapPositionCenter = mapPositionNew;
    if( Data.get(this, 'noAddressUpdate') ) {
      Data.print('ParkistMap.js.onRegionChangeComplete NO ADDRESS UPDATE!');

      return this.noAddressUpdate = false;
    }

    let center = Screen.center();
    center.y += MARKER_SIZE / 2 * .9; // why? because: 1. marker pointer is not at the map center; 2. marker graphics does not corresponds to marker image therefore .9
    const self = this;

    //Data.print('ParkistMap.js.onRegionChangeComplete', Screen.width() + 'x' + Screen.height());
    //Data.print('ParkistMap.js.onRegionChangeComplete', center);

    // relative point position for android
    if( Platform.OS === 'android' ) {
      center.x /= Screen.width();
      center.y /= Screen.height();
    }

    this._map._component.coordinateForPoint(center, (error, coords) => {
      Data.print('ParkistMap.js.onRegionChangeComplete.coordinateForPoint', coords);      
      
      if(error)
        coords = mapPositionNew;

      self.mapPosition = coords;
      self.geocodeReverse(coords);
    });
  }

  // called on Android unstead of coordinateForPoint callback
  onPointToCoordComplete(coords) {
    //Data.print('ParkistMap.js.onPointToCoordComplete', coords);

    this.mapPosition = coords;
    this.geocodeReverse(coords);
  }

  // transfer user to his location
  onLocationPress() {
    var mapPositionNew = {};
    mapPositionNew.latitude = this.lastUserPosition.latitude;
    mapPositionNew.longitude = this.lastUserPosition.longitude;
    mapPositionNew.latitudeDelta = this.mapPosition.latitudeDelta;
    mapPositionNew.longitudeDelta = this.mapPosition.longitudeDelta;

    //Data.print('Позиция', mapPositionNew);

    this.moveMapTo(mapPositionNew);
  }

  // map touches
  onTouchStart(event) {
    //Data.print('Прикосновение начало', event);
    
    if( !this.state.isMapDraggedManually ) {
      const isMapDraggedManually = true;
      this.setState({isMapDraggedManually, address: ''});
      this.mapPositionOnTouchStart = this.mapPosition;
    }
  }

  onTouchEnd(event) {
    //Data.print('ParkistMap.onTouchEnd');
    
    if( this.state.isMapDraggedManually || this.state.address === '' ) {
      const isMapDraggedManually = false;
      this.setState({isMapDraggedManually});

      // update address ontap (no position change)
      if( this.mapPosition === this.mapPositionOnTouchStart && this.state.address === '' )
        this.geocodeReverse(this.mapPosition);
    }
  }

  // select contact type
  onContactPress() {
    const options = [I18n.t('contact_phone'), I18n.t('contact_email'), I18n.t('contact_cancel')];
    const cancelButtonIndex = 2;
    
    ActionSheet.showActionSheetWithOptions({
      options,
      cancelButtonIndex,
    },
    (buttonIndex) => {
      const contactPhone = typeof CONTACT_PHONE !== 'undefined' ? CONTACT_PHONE : '';
      const contactEmail = typeof CONTACT_EMAIL !== 'undefined' ? CONTACT_EMAIL : '';

      switch (buttonIndex) {
        case 0:
          if( contactPhone.length == 0 )
            return Alert.alert('', I18n.t('contact_phone_not_set'));

          Communications.phonecall(contactPhone, true);

          break;
        
        case 1:
          if( contactEmail.length == 0 )
            return Alert.alert('', I18n.t('contact_email_not_set'));

          Communications.email([contactEmail], null, null, I18n.t('contact_email_subject'), null);

          break;
      }
    })
  }

  // TODO: make reverse geocode queue 
  isGeocodeRunning = false;

  // get street by coords
  geocodeReverse(position) {
    //Data.print('ParkistMap.js.geocodeReverse');

    if( this.isGeocodeRunning ) {
      Data.print('ParkistMap.js.geocodeReverse IS RUNNING ALREADY');

      return;
    }

    this.isGeocodeRunning = true;

    const latitude = Data.get(position, 'latitude');
    const longitude = Data.get(position, 'longitude');

    const pos = {
      lat: latitude,
      lng: longitude
    };

    Geocoder.geocodePosition(pos, I18n.locale).then(res => {
      //Data.print('Геокодирование завершено', res);
   
      const address = Data.get(Data.geocodeResultsPrepare(res).shift(), 'address', '');

      this.setState({address});

      this.isGeocodeRunning = false;
    })
    .catch(err => {
      Data.print('Ошибка обратного геокодирования', err)

      this.isGeocodeRunning = false;
    });
  }

  // returning from address refine view
  addressRefined(data) {
    //Data.print('ParkistMap.addressRefined', data);

    if( Data.get(data, 'address') )
      this.setState({address: data.address});

    if( Data.get(data, 'latitude') && Data.get(data, 'longitude') )
      this.moveMapTo(data, true);
  }

  // refine address
  onParkistAddressPress() {
    this.props.onAddressRefine({ address: this.state.address });
  }

  // summon parkist
  onParkistRequestPress() {
    //Data.print('Атрибуты', this.props);

    this.props.onRequest({ 
      address: this.state.address,
      coords: this.mapPosition.latitude + ', ' + this.mapPosition.longitude,
      mapLink: 'http://www.google.com/maps/place/' + this.mapPosition.latitude + ',' + this.mapPosition.longitude,
    });
  }

  // show map
  render() {
    //Data.print('Позиция карты', this.state.mapPosition);
    //Data.print('Карта двигается программно', this.state.isMapDraggedProgrammatically);
    //Data.print('Текущий адрес', this.state.address);
    //Data.print('Длина', this.state.address.length);

    return (

      <View style={style.defaultContainer}>

        {/* HEADER */}
        <View style={[style.header, style.mapHeader]}>
          <TouchableOpacity onPress={ () => this.onContactPress() }>
            <Icon name="phone" style={[style.headerButton, style.defaultShadow]} />
          </TouchableOpacity>

          { true && Data.get(this, 'lastUserPosition') &&  // display location icon only if lastPosition is set

          <TouchableOpacity onPress={ () => this.onLocationPress() }>
            <Icon name="location-arrow" style={[style.headerButton, style.defaultShadow]} />
          </TouchableOpacity>
          
          }

        </View>

        { !Data.get(this.state, 'isMapDraggedManually') && this.state.address.length > 0 &&

        <TouchableOpacity style={style.addressWrapper} onPress={ () => this.onParkistAddressPress() }>
          <Text style={style.address}>{this.state.address}</Text>
          <Text style={[style.address, style.buttonAddress]}>{I18n.t('refine_address')}</Text>
        </TouchableOpacity>
        
        }

        { !Data.get(this.state, 'isMapDraggedManually') && this.state.address.length > 0 &&

        <View style={[style.submitButtonWrapper, style.parkistButtonWrapper]}>
          <TouchableOpacity style={[style.submitButton]} onPress={ () => this.onParkistRequestPress() }>
            <Image style={style.buttonIcon} source={require('../assets/parkist-ico.png')} />
            <Text style={style.buttonText}>{I18n.t('summon_parkist')}</Text>
          </TouchableOpacity>
        </View>

        }

        {/* MAP */}
        <MapView.Animated 
          provider={MapView.PROVIDER_GOOGLE}
          loadingEnabled={true}
          style={style.map} 
          initialRegion={oStartLocation}
          onRegionChange={ (mapPosition) => this.onRegionChange(mapPosition) }
          onRegionChangeComplete={ (mapPosition) => this.onRegionChangeComplete(mapPosition) }
          onPointToCoord={ (coords) => this.onPointToCoordComplete(coords) }
          onTouchStart={ (event) => this.onTouchStart(event) }
          onTouchEnd={ (event) => this.onTouchEnd(event) }
          ref={(map) => {this._map = map}} >
        </MapView.Animated>

        {/* ICON */}
        <Icon pointerEvents="none" name="map-marker" style={[style.marker, style.defaultShadow]} />

      </View>

    );
  }
}