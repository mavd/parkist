/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
'use strict';

import React, { Component } from 'react';
import {
  View,
  ScrollView,
  Text,
  TouchableOpacity,
  ActivityIndicator,
  Alert,
  AsyncStorage
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

// data helper
import Data from '../helpers/data_helper';

// parkist request model
import ParkistRequestModel from '../models/parkist_request_model';

// form import
import {
  form,
  String as fString,
  Date as fDate,

  maybe,
  refinement,
  struct,
} from 'tcomb-form-native';

// no ass time
const moment = require('moment');

// main style
const style = require('../styles/main_style.js');

// form style
const formStyle = require('../styles/form_style.js');

// translations
const I18n = require('../assets/i18n.js');

// form component
const Form = form.Form;

// phone refinement
const fPhone = refinement(fString, function (string) {
  return /^[0-9\-\+\(\)]+$/.test(string);
});

// request form model
const Request = struct({
  address: fString,
  coords: fString,
  mapLink: fString, 
  type: fString,
  name: fString, 
  phone: fPhone,
  time: maybe(fDate),
  comment: maybe(fString), 
});

 // form options
const options = {
  fields: {
    address: {
      label: I18n.t('label_address') + '*', 
    },
    coords: {
      label: I18n.t('label_coords') + '*',
      hidden: true,
    },
    mapLink: {
      label: I18n.t('label_link') + '*',
      hidden: true,
    },
    type: {
      label: I18n.t('label_type') + '*', 
    },
    name: {
      label: I18n.t('label_name') + '*', 
    },
    phone: {
      label: I18n.t('label_phone') + '*',
      keyboardType: 'phone-pad',
    },
    time: {
      label: I18n.t('label_time'),
      mode: 'time',
      config: {
        format: function(date) {
          //const currentDate = new Date();
          //const minutes = Math.floor((((date - currentDate) % 86400000) % 3600000) / 60000);

          const minutes = moment(date).diff(moment(), 'minutes');

          return moment(date).format('HH:mm');
          //return Math.abs(minutes) < 10 ? I18n.t('due_time_now') : moment(date).format('HH:mm');
        }
      }
    },
    comment: {
      label: I18n.t('label_comment'), 
    },
  },

  stylesheet: formStyle,
};

export default class ParkistRequest extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      container: {},
      submission: false,
    }
  }

  componentDidMount() {
    const data = this.props.data;

    // restore form values
    AsyncStorage
      .multiGet(['type', 'name', 'phone'], (error, values) => {
        //Data.print('Error', error);
        //Data.print('Restored', values);

        let formValues = {};
        formValues.time = moment().add(30, 'minutes').toDate();
        formValues.address = data.address;
        formValues.coords = data.coords;
        formValues.mapLink = data.mapLink;

        values.map((result, i) => {
          let key = values[i][0];
          let value = values[i][1];

          formValues[key] = value;
        });
        
        this.setState({ value: formValues });
      });
  }

  onReturn() {
    //this.setState({ container: {}});
    this.props.onReturn();
  }

  onChange(value) {
    this.setState({value});
  }

  // form submit
  onSubmit() {
    if( Data.get(this, 'isWaitingForSubmissionResults') )
      return;
    this.isWaitingForSubmissionResults = true;

    const data = this._form.getValue();
    
    // save form values 

    if (!data) {
      this.isWaitingForSubmissionResults = false;
      return;
    }

    AsyncStorage.multiSet([
      ['type', data.type],
      ['name', data.name],
      ['phone', data.phone],
      ], (error) => {
        //Data.print('ParkistRequest.AsyncStorage.multiSet', error);
      });

    // create request
    var request = {};
    request.address = data.address;
    request.coords = data.coords;
    request.mapLink = data.mapLink;
    request.type = data.type;
    request.name = data.name;
    request.phone = data.phone.replace(/[^0-9]/g, '');
    request.time = moment(data.time).diff(moment(), 'minutes') < -10 ? moment(data.time).add(24, 'hour').format('DD.MM.YYYY в HH:mm') : moment(data.time).format('DD.MM.YYYY в HH:mm');
    request.comment = data.comment;

    // send request
    this.setState({submission: true});
    new ParkistRequestModel()
      .create(request)
      .then( (parkistRequest) => {
        //Data.print('ParkistRequest.onSubmit', request);

        if( !parkistRequest ) {
          Alert.alert('', I18n.t('parkist_request_error'));
        } else {
          Alert.alert('', I18n.t('parkist_request_created'));

          this.onReturn();
        }

        this.setState({submission: false});
        this.isWaitingForSubmissionResults = false;
        return true;
      })
      .catch((error) => {
        Data.print('ParkistRequest.onSubmit', error);
        
        this.setState({submission: false});
        this.isWaitingForSubmissionResults = false;
        return null;
      })
  }

  // show form
  render() {
    
    return (

      <View style={[style.defaultContainer, style.requestContainer, this.state.container]}>
        
        {/* HEADER */}
        <View style={style.header}>
          <TouchableOpacity onPress={ () => this.onReturn() }>
            <Icon name="chevron-left" style={[style.headerButton, style.defaultShadow]} />
          </TouchableOpacity>

          <Text style={style.title}>{this.props.title}</Text>

          <Icon name="question" style={[style.headerButton, style.invisible]} />
        </View>

        <KeyboardAwareScrollView extraScrollHeight={30} showsVerticalScrollIndicator={false} style={style.scroll}>
          {/* FORM */}
          <Form
            type={Request}
            options={options}
            value={this.state.value}
            onChange={(value) => this.onChange(value)}
            ref={(form) => { this._form = form}} />

          {/* SUBMIT */}
          <View style={style.submitButtonWrapper}>

            { !this.state.submission &&

            <TouchableOpacity style={[style.submitButton, style.requestButton]} onPress={ () => this.onSubmit() }>
              <Icon name="check-circle-o" style={[style.buttonText, style.buttonIcon]} />
              <Text style={style.buttonText}>{I18n.t('form_submit')}</Text>
            </TouchableOpacity>
            
            }

            { this.state.submission &&

            <ActivityIndicator
              style={style.requestIndicator}
              color={'#13519f'}
              animating={true}
              size="large"/>

            }

          </View>
        </KeyboardAwareScrollView>

      </View>

    )
  }
}